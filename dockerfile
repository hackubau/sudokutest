# Dockerfile

FROM openjdk:8

LABEL Author="Guasso bau"

EXPOSE 8191

ARG BASE="/src/app/sudokuBau/"

# Create app directory
WORKDIR ${BASE}

COPY ./target/sudoky*.jar ./sudoky.jar
COPY ./src/main/resources/application.yml ./application.yml

CMD [ "java", "-jar","sudoky.jar" ]