package it.hackubau.sudoky;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SudokyApplication {

	public static void main(String[] args) {
		SpringApplication.run(SudokyApplication.class, args);
	}

}
