package it.hackubau.sudoky.adapters;


import it.hackubau.sudoky.enums.Difficulty;

import java.beans.PropertyEditorSupport;


public class DifficultyAdapter extends PropertyEditorSupport {

    @Override
    public void setAsText(final String text) throws IllegalArgumentException {
        setValue(Difficulty.getByValue(text));
    }

}
