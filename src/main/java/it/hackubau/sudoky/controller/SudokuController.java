package it.hackubau.sudoky.controller;

import it.hackubau.sudoky.adapters.DifficultyAdapter;
import it.hackubau.sudoky.enums.Difficulty;
import it.hackubau.sudoky.gameObjects.Tabellone;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

@org.springframework.web.bind.annotation.RestController
public class SudokuController {


    @InitBinder
    public void initBinder(WebDataBinder dataBinder) {
        dataBinder.registerCustomEditor(Difficulty.class, new DifficultyAdapter());
    }

    @GetMapping("/")
    ModelAndView getSudoku(@ModelAttribute Tabellone tabellone, ModelAndView mav) {

        mav = new ModelAndView("sudoku");
        mav.addObject("tabellone", tabellone.reset(null));

        return mav;
    }

    @GetMapping("/sudoku/web/{difficulty}")
    ModelAndView getSudoku(@PathVariable Difficulty difficulty, @ModelAttribute Tabellone tabellone, ModelAndView mav) {
        mav = new ModelAndView("sudoku");
        mav.addObject("tabellone", tabellone.reset(difficulty));


        return mav;
    }


}
