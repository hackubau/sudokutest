package it.hackubau.sudoky.enums;

import lombok.Getter;
import org.thymeleaf.util.StringUtils;

import java.util.Arrays;

@Getter
public enum Difficulty {

    NEWBY(5), VERY_EASY(15), EASY(27), MEDIUM(45), HARD(58), VERY_HARD(75);

    Integer hiddenCells;

    Difficulty(Integer hiddenCells) {
        this.hiddenCells = hiddenCells;
    }


    public static Difficulty getByValue(String value) {
        return Arrays.stream(Difficulty.values()).filter(x -> StringUtils.equals(x != null ? x.toString() : null, value)).findFirst().orElse(null);
    }
}
