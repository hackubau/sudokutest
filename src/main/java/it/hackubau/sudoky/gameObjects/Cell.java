package it.hackubau.sudoky.gameObjects;

import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
public class Cell {

    Integer userValue = null;
    Integer value = -1;
    Boolean visible = true;
}
