package it.hackubau.sudoky.gameObjects;

import it.hackubau.sudoky.enums.Difficulty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.awt.*;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Data
@NoArgsConstructor
public class Tabellone {
    private final int totalColRows = 9;
    private final int sqrtColRows = Double.valueOf(Math.sqrt(totalColRows)).intValue();

    Cell[][] cells = new Cell[totalColRows][totalColRows];
/*    HashMap<Integer, Point>[] rows = new HashMap[totalColRows];
    HashMap<Integer, Point>[] cols = new HashMap[totalColRows];*/

    public Tabellone(boolean reset, Difficulty difficulty) {
        this.cells = new Cell[totalColRows][totalColRows];
        if (reset) {
            reset(difficulty);
        }
    }

    public Tabellone reset(Difficulty difficulty) {
        difficulty = difficulty != null ? difficulty : Difficulty.MEDIUM;
        initCellsRowsAndCols();
        fillDiagonals();
        fillRemaining(0, sqrtColRows);
        hideCells(difficulty);
        printMatrix(true);
        printMatrix(false);
        return this;
    }

    private void initCellsRowsAndCols() {
        for (int i = 0; i < cells.length; i++) {
            for (int ii = 0; ii < cells.length; ii++) {
                cells[i][ii] = new Cell();
            }
            /*rows[i] = new HashMap<>();
            cols[i] = new HashMap<>();*/
        }
    }


    public Tabellone fillDiagonals() {
        for (int i = 0; i < sqrtColRows; i++) {
            fillSquaresNoCheck(i * sqrtColRows, i * sqrtColRows);

        }
        return this;
    }


    private Tabellone fillSquaresNoCheck(int x, int y) {
        //preparo gli elementi del quadrato da mischiare
        List<Integer> pool = IntStream.range(1, totalColRows + 1).boxed().collect(Collectors.toList());

        //posiziono ogni valueero nel quadrato selezionato estrendolo casualmente dalla pool
        for (int i = 0; i < sqrtColRows; i++) {
            for (int ii = 0; ii < sqrtColRows; ii++) {
                Integer xx = x + i;
                Integer yy = y + ii;
                int index = (int) (Math.random() * ((pool.size())));
                int value = pool.get(index);
                pool.remove(index);
                cells[xx][yy].setValue(value);
/*
                //mi segno che valore c'è in quale riga
                rows[xx].put(value, new Point(xx, yy));
                cols[yy].put(value, new Point(xx, yy));*/

            }
        }
        return this;
    }


    public Tabellone printMatrix(boolean showHidden) {
        for (int i = 0; i < totalColRows; i++) {
            for (int ii = 0; ii < totalColRows; ii++) {
                if (!showHidden && !cells[i][ii].getVisible()) {
                    System.out.print(" ");
                } else {
                    System.out.print(cells[i][ii].getValue());
                }
            }
            System.out.println();
        }
        System.out.println();
        System.out.println("________________");
        System.out.println();
        return this;
    }


    // Returns false if given sqrtColRows x sqrtColRows block contains value.
    boolean unUsedInBox(int rowStart, int colStart, int value) {
        for (int i = 0; i < sqrtColRows; i++)
            for (int j = 0; j < sqrtColRows; j++)
                if (cells[rowStart + i][colStart + j].getValue() == value)
                    return false;

        return true;
    }


    int random(int value) {
        return (int) Math.floor((Math.random() * value + 1));
    }


    boolean noCollision(int i, int j, int value) {
        return (
                safeRows(i, value) &&
                        safeCols(j, value) &&
                        unUsedInBox(i - i % sqrtColRows, j - j % sqrtColRows, value));
    }


    boolean safeRows(int i, int value) {
        for (int j = 0; j < totalColRows; j++)
            if (cells[i][j].getValue() == value)
                return false;
        return true;
    }


    boolean safeCols(int j, int value) {
        for (int i = 0; i < totalColRows; i++)
            if (cells[i][j].getValue() == value)
                return false;
        return true;
    }

    boolean fillRemaining(int i, int j) {
        if (j >= totalColRows && i < totalColRows - 1) {
            i = i + 1;
            j = 0;
        }
        if (i >= totalColRows && j >= totalColRows)
            return true;

        if (i < sqrtColRows) {
            if (j < sqrtColRows)
                j = sqrtColRows;
        } else if (i < totalColRows - sqrtColRows) {
            if (j == (int) (i / sqrtColRows) * sqrtColRows)
                j = j + sqrtColRows;
        } else {
            if (j == totalColRows - sqrtColRows) {
                i = i + 1;
                j = 0;
                if (i >= totalColRows)
                    return true;
            }
        }

        for (int value = 1; value <= totalColRows; value++) {
            if (noCollision(i, j, value)) {
                cells[i][j].setValue(value);
                if (fillRemaining(i, j + 1))
                    return true;

                cells[i][j].setValue(0);
            }
        }
        return false;
    }


    /**
     * Nascondo alcune caselle
     */
    public void hideCells(Difficulty difficulty) {
        int count = difficulty.getHiddenCells();
        while (count != 0) {

            //per una miglior distribuzione del random faccio prima il calcolone su tutte le celle e poi divido
            //per ottenere x y
            int cellId = random(totalColRows * totalColRows);
            int xx = (cellId / totalColRows);
            int yy = cellId % 9;

            if (!(xx > 8 || yy > 8)) {

                // System.out.println(i+" "+j);
                if (cells[xx][yy].getVisible()) {
                    count--;
                    cells[xx][yy].setVisible(false);
                }
            }
        }
    }

    public Point getSquareRootXY(Point p) {
        return getSquareRootXY(Double.valueOf(p.getX()).intValue(), Double.valueOf(p.getY()).intValue());

    }

    public Point getSquareRootXY(Integer x, Integer y) {
        BigDecimal bx = new BigDecimal(x);
        BigDecimal by = new BigDecimal(y);
        BigDecimal tre = new BigDecimal(sqrtColRows);
        Integer xRoot = x == 0 ? x : bx.divide(tre, 0, BigDecimal.ROUND_UP).multiply(tre).subtract(tre).intValue();
        Integer yRoot = y == 0 ? y : by.divide(tre, 0, BigDecimal.ROUND_UP).multiply(tre).subtract(tre).intValue();
        return new Point(xRoot, yRoot);
    }

}