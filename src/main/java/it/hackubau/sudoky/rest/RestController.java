package it.hackubau.sudoky.rest;

import it.hackubau.sudoky.adapters.DifficultyAdapter;
import it.hackubau.sudoky.enums.Difficulty;
import it.hackubau.sudoky.gameObjects.Tabellone;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;

@org.springframework.web.bind.annotation.RestController
public class RestController {


    @InitBinder
    public void initBinder(WebDataBinder dataBinder) {
        dataBinder.registerCustomEditor(Difficulty.class, new DifficultyAdapter());
    }

    @GetMapping("/sudoku/rest/{difficulty}")
    Tabellone sudoku(@PathVariable Difficulty difficulty) {
        return new Tabellone(true, difficulty);
    }


}
